import socket


def system_hostname(request):
    return {
        'system_hostname': socket.gethostname(),
    }
